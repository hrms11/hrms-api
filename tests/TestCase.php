<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $actingUser;

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function setActingUser(): void {
        $this->actingUser = User::factory()->create();
    }

    public function getActingUser(): User {
        return $this->actingUser;
    }
}
