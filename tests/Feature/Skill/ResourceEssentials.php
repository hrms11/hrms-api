<?php

namespace Tests\Feature\Skill;

use Tests\TestCase;

class ResourceEssentials
{
    public $listRouteName = 'skills::list';

    public $resourceListAttributes = [
        'data' => [
            '*' => [
                'id',
                'title',
            ]
        ]
    ];

    public function __construct(TestCase $testCase) {

    }
}
