<?php

namespace Tests\Feature\Skill;

use Database\Seeders\SkillSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\Feature\Skill\ResourceEssentials;
use Tests\TestCase;

class ListTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatListResourceNeedsAuth()
    {
        $response = $this->json('get', route($this->resource->listRouteName));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatCanListResource()
    {
        $this->seed(SkillSeeder::class);
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $response = $this->json('get', route($this->resource->listRouteName));
        $response->assertOk();
        $this->assertJson($response->getContent(), 'Response is JSON');
        $response->assertJsonStructure($this->resource->resourceListAttributes);
    }
}
