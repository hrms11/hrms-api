<?php

namespace Tests\Feature\Staff;

use Database\Seeders\FileSeeder;
use Database\Seeders\SkillSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatCreateResourceNeedsAuth()
    {
        $response = $this->json('post', route($this->resource->createRouteName));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatCreateResourceValidatesInput()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $response = $this->json('post', route($this->resource->createRouteName), $this->resource->resourceInvalidDummyData);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors($this->resource->resourceValidationFields, 'errors');
    }

    public function testThatCanCreateResource()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $this->seed(FileSeeder::class);
        $this->seed(SkillSeeder::class);
        $createData = $this->resource->getCreateData();
        $response = $this->json('post', route($this->resource->createRouteName), $createData);
        $response->assertOk();
        $response->assertJson($this->resource->getCreateDataAssert());
        $response->assertJsonStructure($this->resource->resourceAttributes);
    }
}
