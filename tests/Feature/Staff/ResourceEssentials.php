<?php

namespace Tests\Feature\Staff;

use Tests\TestCase;

class ResourceEssentials
{
    public $createRouteName = 'staff::create';
    public $listRouteName = 'staff::list';
    public $getRouteName = 'staff::get';
    public $updateRouteName = 'staff::update';

    public $resourceAttributes = [
        'id',
        'first_name',
        'last_name',
        'position',
        'min_salary',
        'max_salary',
        'linkedin_url',
        'status',
        'updated_at',
        'created_at',
        'skills' => [
            '*' => [
                'title',
            ]
        ],
        'resume' => [
            'type',
            'path',
            'name',
        ],
    ];

    public $resourceListAttributes = [
        'data' => [
            '*' => [
                'id',
                'first_name',
                'last_name',
                'position',
                'min_salary',
                'max_salary',
                'linkedin_url',
                'resume_id',
                'status',
                'resume',
                'skills',
            ]
        ],
    ];

    public $resourceInvalidDummyData;
    public $resourceDummyData;

    public $resourceValidationFields = [
        'first_name',
        'last_name',
        'position',
        'resume_id',
        'linkedin_url',
        'skill_ids.0',
        'min_salary',
        'max_salary',
    ];

    public function __construct(TestCase $testCase) {
        $this->resourceInvalidDummyData = [
            'first_name' => '',
            'last_name' => '',
            'position' => '',
            'resume_id' => 999,
            'linkedin_url' => \Str::random(151),
            'skill_ids' => [1, 1],
            'min_salary' => -1,
            'max_salary' => \Str::random(6),
        ];

        $this->resourceDummyData = [
            'first_name' => \Str::random(10),
            'last_name' => \Str::random(10),
            'position' => \Str::random(30),
            'min_salary' => 1000,
            'max_salary' => 10000,
            'linkedin_url' => \Str::random(30),
            'skill_ids' => [1, 2],
            'resume_id' => 1,
        ];
    }

    public function getCreateData(): array {
        return $this->resourceDummyData;
    }

    public function getCreateDataAssert(): array {
        $data = $this->resourceDummyData;
        unset($data['skill_ids']);
        return $data;
    }

    public function getUpdateData(): array {
        return $this->resourceDummyData;
    }

    public function getUpdateDataAssert(): array {
        $data = $this->resourceDummyData;
        unset($data['skill_ids']);
        return $data;
    }
}
