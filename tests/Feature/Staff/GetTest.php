<?php

namespace Tests\Feature\Staff;

use Database\Seeders\StaffSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class GetTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatGetResourceNeedsAuth()
    {
        $response = $this->json('get', route($this->resource->getRouteName, ['id' => 1]));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatGetResourceReturnsNotFound()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $response = $this->json('get', route($this->resource->getRouteName, ['id' => 1]));
        $this->assertEquals(404, $response->getStatusCode(), 'Response HTTP code is 404');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatCanGetResource()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $this->seed(StaffSeeder::class);
        $response = $this->json('get', route($this->resource->getRouteName, ['id' => 1]));
        $response->assertOk();
        $this->assertJson($response->getContent(), 'Response is JSON');
        $response->assertJsonStructure($this->resource->resourceAttributes);
    }
}
