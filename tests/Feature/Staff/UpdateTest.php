<?php

namespace Tests\Feature\Staff;

use Database\Seeders\StaffSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatUpdateResourceNeedsAuth()
    {
        $response = $this->json('patch', route($this->resource->updateRouteName, ['id' => 1]));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatUpdateResourceValidatesInput()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $response = $this->json('post', route($this->resource->createRouteName), $this->resource->resourceInvalidDummyData);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors($this->resource->resourceValidationFields, 'errors');
    }

    public function testThatCanUpdateResource()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $this->seed(StaffSeeder::class);
        $updateData = $this->resource->getUpdateData();
        $response = $this->json('patch', route($this->resource->updateRouteName, ['id' => 1]), $updateData);
        $response->assertOk();
        $response->assertJsonStructure($this->resource->resourceAttributes);
        $response->assertJson($this->resource->getUpdateDataAssert());
    }
}
