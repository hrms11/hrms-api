<?php

namespace Tests\Feature\Staff;

use Database\Seeders\StaffSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ListTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatListResourceNeedsAuth()
    {
        $response = $this->json('get', route($this->resource->listRouteName));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatCanListResource()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $this->seed(StaffSeeder::class);
        $response = $this->json('get', route($this->resource->listRouteName));
        $response->assertOk();
        $response->assertJsonStructure($this->resource->resourceListAttributes);
    }
}
