<?php

namespace Tests\Feature\StaffHistory;

use Tests\TestCase;

class ResourceEssentials
{
    public $listRouteName = 'staff::history::list';

    public $resourceListAttributes = [
        'data' => [
            '*' => [
                'id',
                'staff_id',
                'status',
                'comment',
            ]
        ]
    ];
}
