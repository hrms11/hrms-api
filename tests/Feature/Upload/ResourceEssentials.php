<?php

namespace Tests\Feature\Upload;

use Tests\TestCase;

class ResourceEssentials
{
    public $createRouteName = 'uploads::store';

    public $resourceAttributes = [
        'id',
        'type',
        'path',
        'name',
    ];

    public function __construct(TestCase $testCase) {

    }
}
