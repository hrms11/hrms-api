<?php

namespace Tests\Feature\Upload;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->resource = new ResourceEssentials($this);
        $this->setActingUser();
    }

    public function testThatCreateResourceNeedsAuth()
    {
        $response = $this->json('post', route($this->resource->createRouteName));
        $this->assertEquals(401, $response->getStatusCode(), 'Response HTTP code is 401');
        $this->assertJson($response->getContent(), 'Response is JSON');
    }

    public function testThatCreateResourceValidatesInput()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $response = $this->json('post', route($this->resource->createRouteName), []);
        $response->assertStatus(422);
    }

    public function testThatCanCreateResource()
    {
        Sanctum::actingAs(
            $this->getActingUser(),
            ['*']
        );
        $createData = UploadedFile::fake()->create('document.pdf', 7);
        $response = $this->json('post', route($this->resource->createRouteName), ['file' => $createData]);
        $response->assertStatus(201);
        $response->assertJsonStructure($this->resource->resourceAttributes);
    }
}
