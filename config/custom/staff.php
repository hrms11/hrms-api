<?php

return [
    'initial_status' => 'initial',
    'statuses' => [
        'initial' => 'Initial',
        'first_contact' => 'First contact',
        'interview' => 'Interview',
        'tech_assignment' => 'Tech assignment',
        'rejected' => 'Rejected',
        'hired' => 'Hired',
    ],
    'list' => [
        'per_page' => 30,
        'time' => 30,
    ],
];
