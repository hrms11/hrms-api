<?php

use App\Http\Controllers\Api\V1\SkillController;
use App\Http\Controllers\Api\V1\StaffController;
use App\Http\Controllers\Api\V1\StaffHistoryController;
use App\Http\Controllers\Api\V1\UploadController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/skills', [SkillController::class, 'index'])->name('skills::list');

    Route::get('/staff', [StaffController::class, 'index'])->name('staff::list');
    Route::post('/staff', [StaffController::class, 'create'])->name('staff::create');
    Route::get('/staff/{id}', [StaffController::class, 'get'])->name('staff::get');
    Route::get('/staff/{id}/timeline', [StaffHistoryController::class, 'index'])->name('staff::history::list');
    Route::patch('/staff/{id}', [StaffController::class, 'update'])->name('staff::update');

    Route::post('/uploads', [UploadController::class, 'store'])->name('uploads::store');
});

// for a testing purpose
// get first user auth token
Route::get('/token', function () {
    $user = User::first();
    if ($user) {
        $token = $user->createToken('test');
        return $token->plainTextToken;
    }
});
