<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('files')->insert([
            [
                'type' => 'resume',
                'path' => '/test/path/',
                'name' => 'resume.pdf',
            ],
        ]);
    }
}
