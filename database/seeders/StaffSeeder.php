<?php

namespace Database\Seeders;

use App\Models\Staff;
use App\Models\StaffHistory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new SkillSeeder)->run();
        (new FileSeeder)->run();


        $staff = new Staff();
        $staff->fill([
            'first_name' => 'Mako',
            'last_name' => 'Bitsadze',
            'position' => 'HR',
            'min_salary' => '10000',
            'max_salary' => '20000',
            'linkedin_url' => 'https://linkedin.com',
            'resume_id' => 1,
            'status' => 'hired',
        ]);

        $staff->save();
        $staff->skills()->attach([1, 2]);

        $sh = new StaffHistory;
        $sh->fill([
            'status' => 'initial',
        ]);
        $staff->history()->save($sh);
    }
}
