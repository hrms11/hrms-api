<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('skills')->insert([
            [ 'title' => 'php'],
            [ 'title' => 'javascript'],
            [ 'title' => 'react'],
            [ 'title' => 'golang'],
            [ 'title' => 'angular'],
            [ 'title' => 'sql'],
            [ 'title' => 'java'],
            ]);
    }
}
