# HRMS API

### Setup

>./vendor/bin/sail up
>
>php artisan migrate
>
>php artisan db:seed

### Run tests

>php artisan test

### Generate swagger

>php artisan l5-swagger:generate


### * Note
*Each endpoint requires authentication. For a testing purpose you can fetch 
this endpoint to get access token.* 

> **GET** {BASE_URL}/token
