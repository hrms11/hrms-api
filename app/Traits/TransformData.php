<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\ParameterBag;

trait TransformData
{
    public function prepareFillableData(ParameterBag $data): ParameterBag
    {
        $prepared = new ParameterBag();
        foreach ($this->fillable as $k => $v) {
            if ($data->has($v)) {
                $prepared->set($v, $data->get($v));
            }
        }
        return $prepared;
    }
}
