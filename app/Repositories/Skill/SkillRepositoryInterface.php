<?php

namespace App\Repositories\Skill;

use Symfony\Component\HttpFoundation\ParameterBag;

interface SkillRepositoryInterface
{
    public function find(ParameterBag $filter);
}
