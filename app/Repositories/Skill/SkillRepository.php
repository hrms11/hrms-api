<?php

namespace App\Repositories\Skill;

use App\Models\Skill;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class SkillRepository implements SkillRepositoryInterface
{

    public function find(ParameterBag $filters): LengthAwarePaginator
    {
        $query = Skill::query();
        $query->whereNull('deleted_at');

        $filters = $filters->all();
        foreach ($filters as $k => $v) {
            switch ($k) {
                case 'search':
                    if (strlen($v) > 2) {
                        $query->where('title', 'like', '%' . $v . '%');
                    }
                    break;
            }
        }

        $query->orderBy('title', 'asc');
        return $query->paginate(Config('custom.skill.list.per_page'));
    }
}
