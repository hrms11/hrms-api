<?php

namespace App\Repositories\Skill;

use App\Traits\CacheableRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class SkillCacheRepository implements SkillRepositoryInterface
{
    use CacheableRepository;

    protected $skillRepository;

    public function __construct(SkillRepository $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    public function find(ParameterBag $filters): LengthAwarePaginator
    {
        return $this->remember(
            'skill_list',
            [$filters->all()],
            config('custom.cache.skill.list.time'),
            function () use ($filters) {
                return $this->skillRepository->find($filters);
            }
        );
    }
}
