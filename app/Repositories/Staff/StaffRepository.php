<?php

namespace App\Repositories\Staff;

use App\Exceptions\DbException;
use App\Exceptions\NotFoundException;
use App\Models\Staff;
use App\Models\StaffHistory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffRepository implements StaffRepositoryInterface
{
    public function find(ParameterBag $filters): LengthAwarePaginator
    {
        $query = Staff::query();
        $query->with(['resume', 'skills']);

        $filters = $filters->all();
        foreach ($filters as $k => $v) {
            switch ($k) {
                case 'status':
                    if (is_array($v)) {
                        $query->whereIn('status', $v);
                    }
                    break;
            }
        }

        $query->orderBy('id', 'desc');
        return $query->paginate(Config('custom.staff.list.per_page'));
    }

    /**
     * @throws NotFoundException
     */
    public function get(int $id, ParameterBag $options = null): ?Staff
    {
        if (!$options) {
            $options = new ParameterBag();
        }

        $relationships = $options->get('relations', []);

        $staff = Staff::with($relationships)->where('id', $id)->first();
        if (!$staff) {
            throw new NotFoundException('Staff not found', 404);
        }
        return $staff;
    }

    /**
     * @throws DbException
     */
    public function create(ParameterBag $data, ParameterBag $options): Staff
    {
        $staffData = $data->all();

        if ($data->has('resume_id')) {
            $staffData['resume_id'] = $data->getInt('resume_id');
        }

        $staff = new Staff;
        $staff->fill($staffData);

        if (!$staff->save()) {
            throw new DbException('Can not create resource');
        }

        $sh = new StaffHistory;
        $sh->fill([
            'status' => $data->get('status')
        ]);
        $staff->history()->save($sh);

        if ($options->has('skill_ids')) {
            $staff->skills()->attach($options->get('skill_ids'));
        }

        return $staff->fresh()->load(['skills', 'resume']);
    }

    public function update($id, ParameterBag $data, ParameterBag $options)
    {
        $staff = $this->get($id);
        $staff->update($data->all());

        if ($options->has('hasNewStatus')) {
            $sh = new StaffHistory;
            $sh->fill([
                'status' => $data->get('status'),
                'comment' => $options->get('comment'),
            ]);
            $staff->history()->save($sh);
        }

        return $staff->fresh()->load(['skills', 'resume']);
    }
}
