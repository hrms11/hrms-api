<?php

namespace App\Repositories\Staff;

use App\Models\Staff;
use App\Traits\CacheableRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffCacheRepository implements StaffRepositoryInterface
{
    use CacheableRepository;

    protected $staffRepository;

    public function __construct(StaffRepository $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    public function find(ParameterBag $filters): LengthAwarePaginator
    {
        return $this->remember(
            'staff_list',
            [$filters->all()],
            config('custom.cache.staff.list.time'),
            function () use ($filters) {
                return $this->staffRepository->find($filters);
            }
        );
    }

    public function get(int $id, ParameterBag $options = null): Staff
    {
        $methodName = 'item_' . $id;
        $optionKeys = $options ? $options->all() : [];
        return $this->remember(
            $methodName,
            [$id, ...$optionKeys],
            config('custom.cache.staff.item.time'),
            function () use ($id, $options) {
                return $this->staffRepository->get($id, $options);
            }
        );
    }

    public function create(ParameterBag $data, ParameterBag $options): Staff
    {
        $staff = $this->staffRepository->create($data, $options);

        $this->clearCacheByTags([
            'staff_list',
        ]);

        return $staff;
    }

    public function update($id, ParameterBag $data, ParameterBag $options)
    {
        $update = $this->staffRepository->update($id, $data, $options);

        $this->clearCacheByTags([
            'staff_list',
            'item_' . $id,
        ]);

        return $update;
    }
}
