<?php

namespace App\Repositories\Staff;

use App\Models\Staff;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

interface StaffRepositoryInterface
{
    public function find(ParameterBag $filters): LengthAwarePaginator;
    public function get(int $id, ParameterBag $options): ?Staff;
    public function create(ParameterBag $data, ParameterBag $options): Staff;
    public function update(int $id, ParameterBag $data, ParameterBag $options);
}
