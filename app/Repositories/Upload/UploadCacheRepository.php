<?php

namespace App\Repositories\Upload;

use App\Models\File;
use App\Traits\CacheableRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

class UploadCacheRepository implements UploadRepositoryInterface
{
    use CacheableRepository;

    protected $uploadRepository;

    public function __construct(UploadRepository $uploadRepository)
    {
        $this->uploadRepository = $uploadRepository;
    }

    public function create(ParameterBag $data): File
    {
        $file = $this->uploadRepository->create($data);

        $this->clearCacheByTags([
            'file_list',
        ]);

        return $file;
    }
}
