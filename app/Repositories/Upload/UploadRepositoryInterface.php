<?php

namespace App\Repositories\Upload;

use App\Models\File;
use Symfony\Component\HttpFoundation\ParameterBag;

interface UploadRepositoryInterface
{
    public function create(ParameterBag $data): File;
}
