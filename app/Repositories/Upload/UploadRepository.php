<?php

namespace App\Repositories\Upload;

use App\Models\File;
use Symfony\Component\HttpFoundation\ParameterBag;

class UploadRepository implements UploadRepositoryInterface
{

    public function create(ParameterBag $data): File
    {
        $filedata = [
            'type' => $data->get('type'),
            'path' => $data->get('path'),
            'name' => $data->get('name'),
        ];
        $file = new File();
        $file->fill($filedata);
        $file->save();

        return $file;
    }
}
