<?php

namespace App\Repositories\StaffHistory;

use App\Traits\CacheableRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffHistoryCacheRepository implements StaffHistoryRepositoryInterface
{
    use CacheableRepository;

    protected $staffHistoryRepository;

    public function __construct(StaffHistoryRepository $staffHistoryRepository)
    {
        $this->staffHistoryRepository = $staffHistoryRepository;
    }

    public function find(int $staff_id, ParameterBag $filters = null): LengthAwarePaginator
    {
        $optionKeys = $filters ? $filters->all() : [];
        return $this->remember(
            'staff_history_list',
            [$staff_id, ...$optionKeys],
            config('custom.cache.staff_history.list.time'),
            function () use ($staff_id, $filters) {
                return $this->staffHistoryRepository->find($staff_id, $filters);
            }
        );
    }
}
