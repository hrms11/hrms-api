<?php

namespace App\Repositories\StaffHistory;

use App\Models\StaffHistory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffHistoryRepository implements StaffHistoryRepositoryInterface
{

    public function find(int $staff_id, ?ParameterBag $filters): LengthAwarePaginator
    {
        $query = StaffHistory::query();
        $query->where('staff_id', $staff_id);

        $query->orderBy('id', 'asc');
        return $query->paginate(Config('custom.staff_history.list.per_page'));
    }
}
