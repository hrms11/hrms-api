<?php

namespace App\Repositories\StaffHistory;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\ParameterBag;

interface StaffHistoryRepositoryInterface
{
    public function find(int $staff_id, ?ParameterBag $filters): LengthAwarePaginator;
}
