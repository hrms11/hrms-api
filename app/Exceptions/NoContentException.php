<?php

namespace App\Exceptions;

use Exception;

class NoContentException extends Exception
{
    public function render($request)
    {
        return response()->json(["error" => true, "message" => $this->getMessage()], 204);
    }
}
