<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\Skill\SkillService;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    protected $skillService;

    public function __construct(SkillService $skillService)
    {
        $this->skillService = $skillService;
    }

    /**
     * @OA\Get(
     *  path="/skills",
     *  summary="Skills list",
     *  description="Retrieve skills",
     *  tags={"Skills"},
     *  security={{"bearerAuth":{}}},
     *  @OA\Parameter(
     *      name="search",
     *      description="Search skill",
     *      example="php",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     *  @OA\Parameter(
     *      name="page",
     *      description="Page",
     *      example="1",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *  @OA\Response(
     *      response=200,
     *      description="Success response",
     *  ),
     *  @OA\Response(
     *      response=401,
     *      description="Unauthorised",
     *  ),
     * )
     */
    public function index(Request $request)
    {
        return $this->skillService->find($request->query);
    }
}
