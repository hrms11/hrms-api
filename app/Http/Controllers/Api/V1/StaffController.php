<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\Staff\StaffService;
use App\Validators\Staff\StoreValidator;
use App\Validators\Staff\UpdateValidator;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    protected $staffService;

    public function __construct(StaffService $staffService)
    {
        $this->staffService = $staffService;
    }

    /**
     * @OA\Get(
     *  path="/staff",
     *  summary="Staff list",
     *  description="Retrieve staff",
     *  tags={"Staff"},
     *  security={{"bearerAuth":{}}},
     *  @OA\Parameter(
     *      name="status[]",
     *      description="Status",
     *      example="initial",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     *  @OA\Response(
     *      response=200,
     *      description="Success response",
     *  ),
     *  @OA\Response(
     *      response=401,
     *      description="Unauthorised",
     *  ),
     * )
     */
    public function index(Request $request)
    {
        return $this->staffService->find($request->query);
    }

    /**
     * @OA\Get(
     *  path="/staff/{id}",
     *  summary="Get single staff",
     *  description="Retrieve single staff",
     *  tags={"Staff"},
     *  security={{"bearerAuth":{}}},
     *  @OA\Parameter(
     *      name="id",
     *      description="Staff ID",
     *      example=1,
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *  @OA\Response(
     *      response=200,
     *      description="Success response",
     *  ),
     *  @OA\Response(
     *      response=404,
     *      description="Not found response",
     *  ),
     *  @OA\Response(
     *      response=401,
     *      description="Unauthorised",
     *  ),
     * )
     */
    public function get($id)
    {
        return $this->staffService->getStaff($id);
    }

    /**
     * @OA\Post(
     *     path="/staff",
     *     tags={"Staff"},
     *     summary="Adds a new staff",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="first_name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="position",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="min_salary",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="max_salary",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="linkedin_url",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="resume_id",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="skill_ids",
     *                     type="number[]"
     *                 ),
     *                 example={
     *                     "first_name": "Jessica",
     *                     "last_name": "Smith",
     *                     "position": "Designer",
     *                     "min_salary": "3000",
     *                     "max_salary": "5000",
     *                     "linkedin_url": "linkedin.com/profie/"
     *                  }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result",
     *              value={
     *                     "id": 1,
     *                     "first_name": "Jessica",
     *                     "last_name": "Smith",
     *                     "position": "Designer",
     *                     "min_salary": "3000",
     *                     "max_salary": "5000",
     *                     "linkedin_url": "linkedin.com/profie/"
     *              },
     *              summary="An result object."),
     *         )
     *     )
     * )
     */
    public function create(StoreValidator $validator)
    {
        return $this->staffService->create($validator);
    }

    /**
     * @OA\Patch(
     *     path="/staff/{id}",
     *     tags={"Staff"},
     *     summary="Update staff",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *      name="id",
     *      description="Staff ID",
     *      example=1,
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="first_name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="position",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="min_salary",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="max_salary",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="linkedin_url",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="resume_id",
     *                     type="number"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result",
     *              value={
     *                     "id": 1,
     *                     "first_name": "Jessica",
     *                     "last_name": "Smith",
     *                     "position": "Designer",
     *                     "min_salary": "3000",
     *                     "max_salary": "5000",
     *                     "linkedin_url": "linkedin.com/profie/"
     *              },
     *              summary="An result object."),
     *         )
     *     )
     * )
     */
    public function update($id, UpdateValidator $validator)
    {
        return $this->staffService->update($id, $validator);
    }
}
