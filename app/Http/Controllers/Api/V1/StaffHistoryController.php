<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\Staff\StaffHistoryService;
use Illuminate\Http\Request;

class StaffHistoryController extends Controller
{
    protected $staffHistoryService;

    public function __construct(StaffHistoryService $staffHistoryService)
    {
        $this->staffHistoryService = $staffHistoryService;
    }

    /**
     * @OA\Get(
     *  path="/staff/{id}/timeline",
     *  summary="Fetch staff timeline",
     *  description="Fetch staff timeline",
     *  tags={"Staff"},
     *  security={{"bearerAuth":{}}},
     *  @OA\Parameter(
     *      name="id",
     *      description="Staff ID",
     *      example=1,
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *  @OA\Response(
     *      response=200,
     *      description="Success response",
     *  ),
     *  @OA\Response(
     *      response=404,
     *      description="Not found response",
     *  ),
     *  @OA\Response(
     *      response=401,
     *      description="Unauthorised",
     *  ),
     * )
     */
    public function index($id, Request $request)
    {
        return $this->staffHistoryService->find($id, $request->query);
    }
}
