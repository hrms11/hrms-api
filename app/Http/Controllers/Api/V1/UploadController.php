<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFileRequest;
use App\Services\Upload\UploadService;
use App\Validators\Uploads\StoreValidator;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    protected $uploadService;

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    /**
     * @OA\Post(
     *     path="/uploads",
     *     tags={"Uploads"},
     *     summary="Store new file",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="file",
     *                     type="file",
     *                 ),
     *                 required={"file"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result",
     *              value={
     *                     "id": 1,
     *                     "type": "resume",
     *                     "path": "/path",
     *                     "name": "/resume.pdf"
     *              },
     *              summary="An result object."),
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' => 'required|mimes:pdf|max:2048',
        ]);
        $validator->validate();
        return $this->uploadService->store($request->file('file'));
    }
}
