<?php

namespace App\Providers;

use App\Repositories\Skill\SkillCacheRepository;
use App\Repositories\Skill\SkillRepositoryInterface;
use App\Repositories\Staff\StaffCacheRepository;
use App\Repositories\Staff\StaffRepositoryInterface;
use App\Repositories\StaffHistory\StaffHistoryCacheRepository;
use App\Repositories\StaffHistory\StaffHistoryRepositoryInterface;
use App\Repositories\Upload\UploadCacheRepository;
use App\Repositories\Upload\UploadRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StaffRepositoryInterface::class, StaffCacheRepository::class);
        $this->app->bind(StaffHistoryRepositoryInterface::class, StaffHistoryCacheRepository::class);
        $this->app->bind(UploadRepositoryInterface::class, UploadCacheRepository::class);
        $this->app->bind(SkillRepositoryInterface::class, SkillCacheRepository::class);

        $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
