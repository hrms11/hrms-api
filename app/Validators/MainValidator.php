<?php

namespace App\Validators;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidatesWhenResolvedTrait;
use Validator;

class MainValidator
{

    use ValidatesWhenResolvedTrait;

    private $paramsBag;
    protected $request;

    public function __construct(Request $request)
    {
        $this->paramsBag = $request->request;
        $this->request = $request;
    }

    /**
     * Resolve validator by old method name
     * validate() method renamed from 5.6
     */
    public function validate()
    {
        $this->validateResolved();
    }

    public function rules()
    {
        return [];
    }

    public function request()
    {
        return $this->request;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->getParamsBag()->all();
    }

    /**
     * Get the validator instance
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidatorInstance()
    {
        return Validator::make(
            $this->validationData(),
            $this->rules()
        );
    }

    public function getParamsBag()
    {
        return $this->paramsBag;
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException('This action is unauthorized.');
    }
}
