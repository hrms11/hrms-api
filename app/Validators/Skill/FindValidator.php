<?php

namespace App\Validators\Skill;

use App\Validators\MainValidator;
use Illuminate\Validation\Rule;

class FindValidator extends MainValidator
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'search' => 'max:3',
            'page' => 'numeric',
        ];
    }
}
