<?php

namespace App\Validators\Staff;

use App\Validators\MainValidator;
use Illuminate\Validation\Rule;

class UpdateValidator extends MainValidator
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'min:2|max:30',
            'last_name' => 'min:2|max:30',
            'position' => 'min:2|max:150',
            'min_salary' => 'numeric|gt:0|max:32767',
            'max_salary' => 'numeric|gt:0|max:32767',
            'linkedin_url' => 'min:2|max:150',
            'resume_id' => 'numeric|exists:files,id',
            'status' => [Rule::in(array_keys(Config('custom.staff.statuses')))],
            'comment' => 'max:300',
        ];
    }
}
