<?php

namespace App\Validators\Staff;

use App\Validators\MainValidator;
use Illuminate\Validation\Rule;

class StoreValidator extends MainValidator
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'position' => 'required|max:150',
            'min_salary' => 'numeric|gt:0|max:32767',
            'max_salary' => 'numeric|gt:0|max:32767',
            'linkedin_url' => 'max:150',
            'resume_id' => 'numeric|exists:files,id',
            'skill_ids.*' => 'numeric|distinct|exists:skills,id',
        ];
    }
}
