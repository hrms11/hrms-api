<?php

namespace App\Models;

use App\Traits\TransformData;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory, TransformData;

    protected $table = 'staff';

    protected $fillable = [
        'first_name',
        'last_name',
        'position',
        'min_salary',
        'max_salary',
        'linkedin_url',
        'resume_id',
        'status',
    ];

    public function resume()
    {
        return $this->hasOne(File::class, 'id', 'resume_id');
    }

    public function history()
    {
        return $this->hasMany(StaffHistory::class, 'staff_id', 'id');
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'staff_skill');
    }
}
