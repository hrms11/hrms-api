<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffHistory extends Model
{
    use HasFactory;

    protected $table = 'staff_history';

    protected $fillable = [
        'staff_id',
        'status',
        'comment',
    ];
}
