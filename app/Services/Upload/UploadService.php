<?php

namespace App\Services\Upload;

use App\Models\File;
use App\Repositories\Upload\UploadRepositoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ParameterBag;

class UploadService
{
    protected $uploadRepository;

    public function __construct(UploadRepositoryInterface $uploadRepository)
    {
        $this->uploadRepository = $uploadRepository;
    }

    public function store(UploadedFile $file, ?ParameterBag $type = null): File
    {
        $filename = time() . '_' . $file->getClientOriginalName();
        $filepath = 'uploads';
        $file->storeAs($filepath, $filename);

        $fileData = new ParameterBag();
        $fileData->set('type', 'resume');
        $fileData->set('path', $filepath);
        $fileData->set('name', $filename);

        return $this->uploadRepository->create($fileData);
    }
}
