<?php

namespace App\Services\Staff;

use App\Exceptions\NoContentException;
use App\Models\Staff;
use App\Repositories\Staff\StaffRepositoryInterface;
use App\Validators\Staff\StoreValidator;
use App\Validators\Staff\UpdateValidator;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffService
{
    protected $staffRepository;

    public function __construct(StaffRepositoryInterface $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    public function find(ParameterBag $filters)
    {
        return $this->staffRepository->find($filters);
    }

    public function get($id, ParameterBag $options = null)
    {
        return $this->staffRepository->get($id, $options);
    }

    public function getStaff($id)
    {
        $options = new ParameterBag();
        $options->set('relations', ['resume', 'skills']);
        return $this->get($id, $options);
    }

    public function create(StoreValidator $validator)
    {
        $validator->validate();

        $data = $validator->getParamsBag();
        $createData = (new Staff)->prepareFillableData($data);
        $createData->set('status', Config('custom.staff.initial_status'));

        $options = new ParameterBag();
        if ($data->has('skill_ids')) {
            $options->set('skill_ids', $data->get('skill_ids', []));
        }

        return $this->staffRepository->create($createData, $options);
    }

    /**
     * @throws NoContentException
     */
    public function update(int $id, UpdateValidator $validator)
    {
        $validator->validate();

        $staff = $this->get($id);
        $data = $validator->getParamsBag();

        $updateData = (new Staff)->prepareFillableData($data);
        if (!$updateData->count()) {
            throw new NoContentException('Update data missing');
        }

        $options = new ParameterBag();
        if ($data->has('status') && $staff->status !== $data->get('status')) {
            $options->set('hasNewStatus', true);
            $options->set('comment', $data->get('comment'));
        }

        return $this->staffRepository->update($id, $updateData, $options);
    }
}
