<?php

namespace App\Services\Staff;

use App\Repositories\StaffHistory\StaffHistoryRepositoryInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class StaffHistoryService
{
    protected $staffHistoryRepository;

    public function __construct(StaffHistoryRepositoryInterface $staffHistoryRepository)
    {
        $this->staffHistoryRepository = $staffHistoryRepository;
    }

    public function find(int $staff_id, ?ParameterBag $filters = null)
    {
        return $this->staffHistoryRepository->find($staff_id);
    }
}
