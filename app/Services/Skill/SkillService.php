<?php

namespace App\Services\Skill;

use App\Repositories\Skill\SkillRepositoryInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class SkillService
{
    protected $skillRepository;

    public function __construct(SkillRepositoryInterface $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    public function find(ParameterBag $filters)
    {
        return $this->skillRepository->find($filters);
    }
}
